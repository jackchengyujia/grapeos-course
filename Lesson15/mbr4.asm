;将屏幕第一行的第一个字符显示为‘G’。

org 0x7c00 ;伪指令

mov ah,0x07 ;黑底白字
mov al,'G'

mov bx,0xb800
mov es,bx
mov [es:0],ax ;文本模式显存地址从0xb8000开始

stop: ;标号
hlt
jmp stop 

times 510-($-$$) db 0 ;将从上条指令结束到最后2个字节前的空余字节全部置为0。
db 0x55,0xaa