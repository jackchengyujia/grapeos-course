;定义常量
DISK_BUFFER equ 0x7e00 ;临时存放数据用的缓存区，放到boot程序之后。0x7e00~0x7fff。

org 0x7c00

;初始化段寄存器
mov ax,cs
mov ds,ax ;ds指向与cs相同的段

mov bx,DISK_BUFFER
;向缓存区前3个字节依次写入4、5、6。
mov byte [bx+0],4
mov byte [bx+1],5
mov byte [bx+2],6
;向缓存区最后3个字节依次写入6、5、4。
mov byte [bx+509],6
mov byte [bx+510],5
mov byte [bx+511],4

mov si,DISK_BUFFER
mov edi,1 ;写入硬盘的第2个扇区
call func_write_one_sector

stop:
hlt
jmp stop 

;将内存中的512个字节写入到硬盘的一个指定扇区中（主硬盘控制器主盘）
;输入参数：ds:si，edi。
;ds:si 数据源内存地址
;edi LBA扇区号
;输出参数：无。
func_write_one_sector:
;第1步：检查硬盘控制器状态
mov dx,0x1f7
.not_ready1:
nop ;nop相当于稍息 hlt相当于睡觉
in al,dx ;读0x1f7端口
and al,0xc0 ;第7位为1表示硬盘忙，第6位为1表示硬盘控制器已准备好，正在等待指令。
cmp al,0x40 ;当第7位为0，且第6位为1，则进入下一个步。
jne .not_ready1 ;若未准备好，则继续判断。
;第2步：设置要写入的扇区数
mov dx,0x1f2
mov al,1
out dx,al ;写入1个扇区
;第3步：将LBA地址存入0x1f3~0x1f6
mov eax,edi
;LBA地址7~0位写入端口0x1f3
mov dx,0x1f3
out dx,al
;LBA地址15~8位写入端口写入0x1f4
shr eax,8
mov dx,0x1f4
out dx,al
;LBA地址23~16位写入端口0x1f5
shr eax,8
mov dx,0x1f5
out dx,al
;第4步：设置device端口
shr eax,8
and al,0x0f ;LBA第24~27位
or al,0xe0 ;设置7~4位为1110，表示LBA模式，主盘
mov dx,0x1f6
out dx,al
;第5步：向0x1f7端口写入写命令0x30
mov dx,0x1f7
mov al,0x30
out dx,al
;第6步：检测硬盘状态
.not_ready2:
nop ;nop相当于稍息 hlt相当于睡觉
in al,dx ;读0x1f7端口
and al,0x88 ;第7位为1表示硬盘忙，第3位为1表示硬盘控制器已准备好数据传输。
cmp al,0x08 ;当第7位为0，且第3位为1，进入下一步。
jne .not_ready2 ;若未准备好，则继续判断。
;第7步：向0x1f0端口写数据
mov cx,256 ;每次写入2字节，一个扇区需要写256次。
mov dx,0x1f0
.go_on_write:
mov ax,[si]
out dx,ax
add si,2
loop .go_on_write
ret

times 510-($-$$) db 0
db 0x55,0xaa