org 0x1000

;打印字符串："GrapeOS loader start."
mov si,loader_start_string
mov di,160 ;屏幕第3行显示
call func_print_string

stop:
hlt
jmp stop

;打印字符串函数
;输入参数：ds:si，di。
;输出参数：无。
;si 表示字符串起始地址，以0为结束符。
;di 表示字符串在屏幕上显示的起始位置（0~1999）
func_print_string:
mov ah,0x07 ;ah 表示字符属性 黑底白字
shl di,1 ;乘2（屏幕上每个字符对应2个显存字节）
.start_char: 
mov al,[si]
cmp al,0
jz .end_print
mov [gs:di],ax
inc si
add di,2
jmp .start_char
.end_print:
ret

loader_start_string:db "GrapeOS loader start.",0