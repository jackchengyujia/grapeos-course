[root@CentOS7 VMShare]# dd if=/dev/zero of=/media/VMShare/GrapeOS.img bs=1M count=4
[root@CentOS7 Lesson20]# nasm boot1.asm -o boot1.bin
[root@CentOS7 Lesson20]# nasm data1.asm -o data1.bin
[root@CentOS7 Lesson20]# dd conv=notrunc if=boot1.bin of=/media/VMShare/GrapeOS.img
[root@CentOS7 Lesson20]# dd conv=notrunc if=data1.bin of=/media/VMShare/GrapeOS.img seek=1

C:\Users\CYJ>qemu-system-i386 d:\GrapeOS\VMShare\GrapeOS.img -S -s

[root@CentOS7 Lesson20]# gdb
(gdb) target remote 192.168.10.100:1234
(gdb) x /512bx 0x7e00
(gdb) c
Continuing.
^C
Program received signal SIGINT, Interrupt.
0x00007c16 in ?? ()
(gdb) x /512bx 0x7e00